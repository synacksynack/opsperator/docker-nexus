should_rehash=false
for f in $(find /certs /run/secrets/kubernetes.io/serviceaccount -name '*.crt' 2>/dev/null)
do
    if test -s $f; then
	if test -d /etc/pki/ca-trust/source/anchors; then
	    dir=/etc/pki/ca-trust/source/anchors
	else
	    dir=/usr/local/share/ca-certificates
	fi
	d=`echo $f | sed 's|/|-|g'`
	if ! test -s $dir/kube$d; then
	    if ! cat $f >$dir/kube$d; then
		echo WARNING: failed installing $f certificate authority >&2
	    else
		should_rehash=true
	    fi
	fi
    fi
done
if $should_rehash; then
    if test -d /etc/pki/ca-trust/source/anchors; then
	if ! update-ca-trust; then
	    echo WARNING: failed updating trusted certificate authorities >&2
	fi
    elif ! update-ca-certificates; then
	echo WARNING: failed updating trusted certificate authorities >&2
    fi
fi
unset should_rehash

NEXUS_TRUST_STORE="${NEXUS_TRUST_STORE:-/nexus-data/trusted.jks}"
NEXUS_TRUST_STORE_PASS="${NEXUS_TRUST_STORE_PASS:-changeit}"
if ! test -s "$NEXUS_TRUST_STORE"; then
    echo Provision Keystore
    if ! test -d "$(dirname "$NEXUS_TRUST_STORE")"; then
	mkdir -p $(dirname "$NEXUS_TRUST_STORE")
    fi
    count=0
    for ca in $(find /certs /run/secrets/kubernetes.io/serviceaccount -name '*.crt' 2>/dev/null)
    do
	if ! test -s "$ca"; then
	    continue
	fi
	old=0
	grep -n 'END CERTIFICATE' "$ca"  | awk -F: '{print $1}' \
	    | while read stop
	    do
		count=`expr $count + 1`
		echo "Processing $ca (#$count)"
		head -$stop "$ca" | tail -`expr $stop - $old` >/tmp/insert.crt
		keytool -import -trustcacerts -alias inter$count \
		    -file /tmp/insert.crt -keystore "$NEXUS_TRUST_STORE" \
		    -storepass "$NEXUS_TRUST_STORE_PASS" -noprompt
		old=$stop
	    done
	echo done with "$ca"
    done
    rm -f /tmp/insert.crt
    unset count old
fi
