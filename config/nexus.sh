#!/bin/sh
set -e

if test "$DEBUG"; then
    set -x
fi

. /usr/local/bin/reset-tls.sh

NEXUS_TRUST_STORE="${NEXUS_TRUST_STORE:-/nexus-data/trusted.jks}"
NEXUS_TRUST_STORE_PASS="${NEXUS_TRUST_STORE_PASS:-changeit}"
if test -s "$NEXUS_TRUST_STORE"; then
    INSTALL4J_ADD_VM_PARAMS="$INSTALL4J_ADD_VM_PARAMS -Djavax.net.ssl.trustStore=$NEXUS_TRUST_STORE -Djavax.net.ssl.trustStorePassword=$NEXUS_TRUST_STORE_PASS"
else
    INSTALL4J_ADD_VM_PARAMS="$INSTALL4J_ADD_VM_PARAMS"
fi

echo Starting Nexus

echo Executing provision.sh
/usr/local/bin/provision.sh &

unset OPENLDAP_BIND_DN_PREFIX OPENLDAP_BIND_PW OPENLDAP_DOMAIN \
    OPENLDAP_HOST OPENLDAP_PROTO OPENLDAP_USERS_OBJECTCLASS \
    OPENLDAP_BASE OPENLDAP_PORT

exec $@
